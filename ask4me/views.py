from django.shortcuts import render, redirect
from .models import StatusModel
from django.http import HttpResponse
# Create your views here.
def indexStatus(request, message_type=''):
    status = StatusModel.objects.order_by('-id')
    return render(request, 'indexStatus.html', {'statuses': status, 'message_type': message_type})


def addStatus(request):
    if request.method == 'POST':
        if request.POST['status'] != '':
            warning = ''
            if request.POST['nama'] != '':
                nama = request.POST['nama']
            else:
                nama = 'Anonim'
                warning = 'STATUSMU AKAN DITAMPILKAN SEBAGAI'
            data = {
                'name': nama,
                'status': request.POST['status'],
                'warning': warning
            }
            return render(request, 'confirmStatus.html', data)
        else:
            return indexStatus(request, message_type='status_empty')
    return redirect('/')


def confirmStatus(request):
    if request.method == 'POST':
        if request.POST['confirm'] == 'yes':
            data = request.POST
            StatusModel.objects.create(nama=data['nama'], pesan=data['status'])
            return indexStatus(request, message_type='success')
    return redirect('/')