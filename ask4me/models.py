from django.db import models

# Create your models here.

class StatusModel(models.Model):
    nama = models.CharField(max_length = 30)
    pesan = models.TextField()

    def __str__(self):
        return self.nama + ' - ' + self.pesan