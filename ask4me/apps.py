from django.apps import AppConfig


class Ask4MeConfig(AppConfig):
    name = 'ask4me'
